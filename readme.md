# University College London Course COMPGI13 - Reinforcement Learning (2015)


Additional information can be found on the David Silver's [course webpage](http://www0.cs.ucl.ac.uk/staff/d.silver/web/Teaching.html).
 - Lectures: [YouTube](https://www.youtube.com/watch?v=2pWv7GOvuf0)
 - Discussion: [Google Groups](http://groups.google.com/group/csml-advanced-topics)


## Assignments

Course work will focus on the practical implementation of Markov decision processes and their solutions. There is only one homework.

 - [ ] Easy 21 Game [`assignment/easy21.pdf`](assignment/easy21.pdf)


## Prerequisites

The prerequisites are probability, calculus, linear algebra and [COMPGI01 Supervised Learning](http://www.cs.ucl.ac.uk/syllabus/compgi/compgi01_supervised_learning/) or [COMPGI08 Graphical Models](http://www.cs.ucl.ac.uk/syllabus/compgi/compgi08_graphical_models/) or [COMPGI18 Probabilistic and Unsupervised Learning](http://www.cs.ucl.ac.uk/syllabus/compgi/compgi18_probabilistic_and_unsupervised_learning/).


## Content

The course will cover Markov decision processes, planning by dynamic programming, monte-carlo methods, model-free prediction and control, value function approximation, policy gradient methods, integration of learning and planning, and the exploration/exploitation dilemma. Possible applications to be discussed include learning to play classic board games as well as video games.
